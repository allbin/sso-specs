const claim_defs = require('./lib/claim_defs');
const feature_defs = require('./lib/feature_defs');
const service_defs = require('./lib/service_defs');
const org_defs = require('./lib/org_defs');
const translations = require('./lib/translations');

function getFeaturesOfOrg(org) {
    var features = {};
    if (org_defs.hasOwnProperty(org) === "false") {
        console.log("ERROR: Invalid organisation, cannot get features.");
        return features;
    }

    org_defs[org].services.forEach(function(service) {
        features[service] = feature_defs[service];
    });

    return features;
}

module.exports = {
    claim_defs: claim_defs,
    feature_defs: feature_defs,
    service_defs: service_defs,
    org_defs: org_defs,
    translations: translations,
    getFeaturesOfOrg: getFeaturesOfOrg
};
