# AllBinary SSO-Admin specifications

## ADDING NEW SERVICES
The following tasks are required when adding a new service:

- add any new features and claims to respective defs
- add translations for all included features, claims and the service name itself
- add service url to service defs
- commit
- update package.json version number, tag commit with version and push
- update package.json for the following projects and re-release them:
- - sso-admin-ui
- - sso-api
- - sso-ui
- - dashboard

---

`index.js` exports [claim_defs](#claim_defs), [feature_defs](#feature_defs), [service_defs](#service_defs), [org_defs](#org_defs), [translations](#translations), and function [getFeatureOfOrg](#getFeatureOfOrg).

## claim_defs
An object with a key for every claim available. Each key is a claim object with a definition with the following props:
```
{
    claim_key: {
        type: <bitfield | boolean | string>,
        bits: <int>,    //Number of bits in bitfield, only used with bitfield type.
        keys: {         //Only used with bitfield type.
            0: <string> //A unique key for that bit, one for every bit specified in <bits>.
            ...
        },
        value: <string> //Value of string if type is string.
    }
}
```

## feature_defs
An object with feature definitions per service. `required claims` are what claims the admin needs to set the specific feature on a user. `granted_claims` are claims that will be granted to the user when given the specific feature.
```
{
    <service>: {
        <feature_key>: {
            required_claims: {
                claim_key: <bool | bitsum | string>,
            },
            granted_claims: {
                claim_key: <bool | bitsum | string>
            }
        }
    }
    ...
    example_service: {
        acme_api_access: {
            required_claims: {
                admin: true
            },
            granted_claims: {
                login: true,
                acme_api_access: 3,
            }
        }
    }
}
```

## service_defs 
An object with one key per service_name. Contains service metadata, such as url.
```
{
    iwa_vl: {
        url: 'https://iwa3.vl.allbin.se'
    }
}
```

## org_defs
An object with one key per organisation. The services specified per organisation is what an admin of a specified organisation can access. Should match the services purchased from AllBinary by the organisation.
```
{
    vl: {
        label: <string>,
        services: [
            'sso_admin',
            'iwa_vl'
        ]
    },
    hlt: {
        label: <string>,
        services: [
            'sso_admin',
            'iwa_hlt'
        ]
    },
    allbinary: {
        label: <string>,
        services: [
            'sso_admin',
            'iwa_vl',
            'iwa_hlt'
        ]
    }
}
```

## translations
Object with three main keys: services, features, claims. Each one itself an object with keys for supported languages.
```
{
    services: {
        "sv-SE": {
            iwa_jlt: "IWA JLT",
            iwa_hlt: "IWA HLT",
        },
        "en-US": {
            iwa_jlt: "IWA JLT",
            iwa_hlt: "IWA HLT",
        }
    },
    claims: {
        "sv-SE": {
            login: "Loginrättigheter",
            admin: "Administratör",
            super_admin: "Huvudadministratör",
        }
        ...
    },
    features: {
        ...
    }
}
```

## getFeaturesOfOrg
`getFeaturesOfOrg(org <string>)` Function that will return only features for the services available to the organisation.
