module.exports = {
  ab_import: {
    url: 'https://localhost',
    icon: 'settings',
    token_lifetime: 48 * 60 * 60,
  },
  allpay: {
    url: 'https://psp.allbin.se',
    icon: 'settings',
  },
  allpay_test: {
    url: 'https://psp.test.allbin.se',
    icon: 'settings',
  },
  bulletin_vl: {
    url: 'https://bulletin.vl.allbin.se',
    icon: 'bulletin',
  },
  bulletin_jlt: {
    url: 'https://bulletin.jlt.allbin.se',
    icon: 'bulletin',
  },
  bulletin_hlt: {
    url: 'https://bulletin.hlt.allbin.se',
    icon: 'bulletin',
  },
  iwa_jlt: {
    url: 'https://iwa3.jlt.allbin.se',
    icon: 'iwa',
  },
  iwa_hlt: {
    url: 'https://iwa3.hlt.allbin.se',
    icon: 'iwa',
  },
  iwa_vl: {
    url: 'https://iwa3.vl.allbin.se',
    icon: 'iwa',
  },
  iwa_lite: {
    url: 'https://iwa3.lite.allbin.se',
    icon: 'iwa',
  },
  iwa_insight: {
    url: 'https://iwa3.insight.allbin.se',
    icon: 'iwa',
  },
  iwa_test: {
    url: 'https://iwa3.test.allbin.se',
    icon: 'iwa',
  },
  iwa: {
    url: 'http://localhost:8080',
    icon: 'iwa',
  },
  disruption_admin_test: {
    url: 'https://disruption-admin.test.allbin.se',
  },
  disruption_admin: {
    url: 'https://disruption-admin.allbin.se',
  },
  disruption_admin_bt: {
    url: 'https://disruption-admin.bt.allbin.se',
  },
  sso_admin: {
    url: 'https://sso-admin.allbin.se',
    icon: 'sso',
  },
  orders_test: {
    url: 'https://orders.test.allbin.se',
    icon: 'orders',
    token_lifetime: 24 * 60 * 60,
  },
  orders_jlt: {
    url: 'https://orders.jlt.allbin.se',
    icon: 'orders',
    token_lifetime: 24 * 60 * 60,
  },
  hamta99: {
    url: 'https://hamta99.allbin.se',
    icon: 'hamta',
    token_lifetime: 30 * 24 * 60 * 60,
  },
  rebus_validation: {
    url: 'https://rebus.allbin.se',
    icon: 'validation',
  },
  rebus_storage_test: {
    url: 'https://rebus.test.allbin.se',
    icon: 'validation',
  },
  rebus_storage: {
    url: 'https://rebus.allbin.se',
    icon: 'validation',
  },
  traffic_lts: {
    url: 'https://traffic.lts.allbin.se',
  },
  stop_analysis_vl: {
    url: 'https://stop-analysis.vl.allbin.se',
    icon: 'some',
  },
  some_vl: {
    url: 'https://workorder.vl.allbin.se',
    icon: 'some',
    token_lifetime: 10 * 60 * 60,
  },
  some_lts: {
    url: 'https://hallplatsservice.allbinary.se',
    icon: 'some',
    token_lifetime: 10 * 60 * 60,
  },
  some_jlt: {
    url: 'https://workorder.jlt.allbin.se',
    icon: 'some',
    token_lifetime: 10 * 60 * 60,
  },
  dashboard: {
    url: 'https://dashboard.allbin.se',
    icon: 'dashboard',
  },
  route_redirection_vl: {
    url: 'https://route-redirection.vl.allbin.se',
    icon: 'redirect',
  },
  route_redirection_test: {
    url: 'https://route-redirection.test.allbin.se',
    icon: 'redirect',
  },
  realtime_transit_map_vl: {
    url: 'https://realtime-transit-map.vl.allbin.se',
    icon: 'map',
  },
  realtime_transit_map_test: {
    url: 'https://realtime-transit-map.test.allbin.se',
    icon: 'map',
  },
  senseview_staging: {
    url: 'https://senseview.staging.ioe.allbin.se',
    icon: 'senseview',
    token_lifetime: 24 * 60 * 60,
  },
  senseview_demo: {
    url: 'https://senseview.demo.ioe.allbin.se',
    icon: 'senseview',
  },
  senseview_wexnet: {
    url: 'https://senseview.wexnet.allbin.se',
    icon: 'senseview',
  },
  senseview_bjarekraft: {
    url: 'https://senseview.bjarekraft.allbin.se',
    icon: 'senseview',
  },
  senseview_consid: {
    url: 'https://senseview.consid.allbin.se',
    icon: 'senseview',
  },
  senseview_ewf: {
    url: 'https://senseview.ewf.allbin.se',
    icon: 'senseview',
  },
  senseview_hoganasenergi: {
    url: 'https://senseview.hoganasenergi.allbin.se',
    icon: 'senseview',
  },
  senseview_halmstadsstadsnat: {
    url: 'https://senseview.halmstadsstadsnat.allbin.se',
    icon: 'senseview',
  },
  senseview_kalmarenergi: {
    url: 'https://senseview.kalmarenergi.allbin.se',
    icon: 'senseview',
  },
  senseview_umeaenergi: {
    url: 'https://senseview.umeaenergi.allbin.se',
    icon: 'senseview',
  },
  senseview_vanerenergi: {
    url: 'https://senseview.vanerenergi.allbin.se',
    icon: 'senseview',
  },
  senseview_karlstad: {
    url: 'https://senseview.karlstad.allbin.se',
    icon: 'senseview',
  },
  senseview_emmabodaenergi: {
    url: 'https://senseview.emmabodaenergi.allbin.se',
    icon: 'senseview',
  },
  senseview_smekab: {
    url: 'https://senseview.smekab.allbin.se',
    icon: 'senseview',
  },
  senseview_ondemand: {
    url: 'https://senseview.ondemand.allbin.se',
    icon: 'senseview',
  },
  senseview_wallenstam: {
    url: 'https://senseview.wallenstam.allbin.se',
    icon: 'senseview',
  },
  url_shortener: {
    url: 'https://allb.in',
  },
};
