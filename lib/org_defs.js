module.exports = {
  allbinary: {
    label: 'All Binary',
    services: [
      'dashboard',
      'disruption_admin',
      'disruption_admin_bt',
      'disruption_admin_test',
      'sso_admin',
      'orders_jlt',
      'orders_test',
      'iwa_vl',
      'iwa_lite',
      'iwa_insight',
      'iwa',
      'iwa_test',
      'stop_analysis_vl',
      'route_redirection_test',
      'route_redirection_vl',
      'realtime_transit_map_test',
      'realtime_transit_map_vl',
      'rebus_validation',
      'rebus_storage',
      'rebus_storage_test',
      'some_vl',
      'some_lts',
      'some_jlt',
      'senseview_staging',
      'senseview_wexnet',
      'senseview_bjarekraft',
      'senseview_consid',
      'senseview_hoganasenergi',
      'senseview_kalmarenergi',
      'senseview_umeaenergi',
      'senseview_vanerenergi',
      'senseview_wallenstam',
      'bulletin_vl',
      'bulletin_hlt',
      'ab_import',
      'url_shortener',
    ],
  },
  allpay: {
    label: 'AllPay',
    services: [
      'allpay',
      'allpay_test',
    ],
  },
  lite: {
    label: 'IWA INsight',
    services: [
      'dashboard',
      'sso_admin',
      'iwa_lite',
      'iwa_insight',
    ],
  },
  vl: {
    label: 'Kollektivtrafikförvaltningen Region Västmanland',
    services: [
      'dashboard',
      'sso_admin',
      'iwa_vl',
      'rebus_validation',
      'stop_analysis_vl',
      'some_vl',
      'bulletin_vl',
    ],
  },
  hlt: {
    label: 'Hallandstrafiken',
    services: ['dashboard', 'sso_admin', 'iwa_hlt', 'bulletin_hlt'],
  },
  jlt: {
    label: 'Jönköpings Länstrafik',
    services: [
      'dashboard',
      'sso_admin',
      'iwa_jlt',
      'some_jlt',
      'orders_jlt',
      'bulletin_jlt',
    ],
  },
  lts: {
    label: 'Sörmlandstrafiken',
    services: ['dashboard', 'sso_admin', 'traffic_lts', 'some_lts'],
  },
  st: {
    label: 'Skånetrafiken',
    services: ['dashboard', 'disruption_admin_test', 'disruption_admin']
  },
  bt: {
    label: 'Blekingetrafiken',
    services: ['dashboard', 'disruption_admin_bt']
  },
  hamta99: {
    label: 'Hämta 99',
    services: ['dashboard', 'hamta99'],
  },
  wexnet: {
    label: 'Wexnet',
    services: ['senseview_wexnet'],
  },
  bjarekraft: {
    label: 'Bjäre Kraft',
    services: ['senseview_bjarekraft'],
  },
  hoganasenergi: {
    label: 'Höganäs Energi',
    services: ['senseview_hoganasenergi'],
  },
  halmstadsstadsnat: {
    label: 'Halmstads Stadsnät',
    services: ['senseview_halmstadsstadsnat'],
  },
  kalmarenergi: {
    label: 'Kalmar Energi',
    services: ['senseview_kalmarenergi'],
  },
  umeaenergi: {
    label: 'Umeå Energi',
    services: ['senseview_umeaenergi'],
  },
  vanerenergi: {
    label: 'VänerEnergi',
    services: ['senseview_vanerenergi'],
  },
  karlstad: {
    label: 'Karlstad El & Stadsnät',
    services: ['senseview_karlstad'],
  },
  consid: {
    label: 'Consid',
    services: ['senseview_staging', 'senseview_consid'],
  },
  ewf: {
    label: 'EWF Eco',
    services: ['senseview_ewf'],
  },
  emmabodaenergi: {
    label: 'Emmaboda Energi',
    services: ['senseview_emmabodaenergi'],
  },
  senseview: {
    label: 'SenseView',
    services: ['senseview_staging', 'senseview_demo'],
  },
  smekab: {
    label: 'Smekab',
    services: ['senseview_smekab']
  },
  ondemand: {
    label: 'OnDemand',
    services: ['senseview_ondemand'],
  },
  wallenstam: {
    label: 'Wallenstam',
    services: ['senseview_wallenstam'],
  }
};
