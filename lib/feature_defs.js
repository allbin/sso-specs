module.exports = {
  sso_admin: {
    admin: {
      required_claims: {
        super_admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        sso_api: 3,
      },
    },
    super_admin: {
      required_claims: {
        allbinary_admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        super_admin: true,
        sso_api: 3,
      },
    },
    allbinary_admin: {
      required_claims: {
        allbinary_admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        super_admin: true,
        allbinary_admin: true,
        sso_api: 3,
      },
    },
  },

  ab_import: {
    rt_import: {
      required_claims: {
        allbinary_admin: true,
      },
      granted_claims: {
        login: true,
        rt_api: 3,
        gtfs_api: 3,
      },
    },
  },

  allpay: {
    readonly: {
      required_claims: {
        allbinary_admin: true,
      },
      granted_claims: {
        psp_api: 1,
      },
    },
  },

  allpay_test: {
    readonly: {
      required_claims: {
        allbinary_admin: true,
      },
      granted_claims: {
        psp_api: 1,
      },
    },
  },

  rebus_storage: {
    read: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rebus_storage_api: 1,
      },
    },
    submit: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rebus_storage_api: 3
      },
    },
  },

  rebus_storage_test: {
    read: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rebus_storage_api: 1,
      },
    },
    submit: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rebus_storage_api: 3
      },
    },
  },

  iwa_insight: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
  },

  iwa_lite: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
  },

  iwa_vl: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_commute_high_school: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 1,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
    doc_access_redirect: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        doc_access: 1 << 4,
      },
    },
    doc_access_delay: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 5,
      },
    },
    doc_access_stoplinks: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 6,
      },
    },
    doc_access_traveltime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 7,
      },
    },
    doc_access_sales: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        econ_api: 1,
        doc_access: 1 << 8,
      },
    },
    doc_access_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 9,
      },
    },
    doc_access_monthly_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 10,
      },
    },
    doc_access_apc: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 11,
      },
    },
    doc_access_realtime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        rt_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 12,
      },
    },
    doc_access_special_passenger_services: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        sps_api: 1,
        doc_access: 1 << 14,
      },
    },
    doc_access_special_passenger_services_ntd: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        sps_api: 1,
        doc_access: 1 << 15,
      },
    },
    doc_access_validation_report: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        econ_api: 1,
        doc_access: 1 << 16,
      },
    },
    doc_access_sps_coverage: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        sps_api: 1,
        doc_access: 1 << 17,
      },
    },
    doc_access_sps_travelers: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        scb_api: 1,
        sps_api: 1,
        doc_access: 1 << 18,
      },
    },
  },

  iwa_hlt: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_commute_high_school: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 1,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
    doc_access_delay: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 5,
      },
    },
    doc_access_stoplinks: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 6,
      },
    },
    doc_access_traveltime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 7,
      },
    },
    doc_access_realtime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        rt_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 12,
      },
    },
  },

  iwa_jlt: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_commute_high_school: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 1,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
    doc_access_redirect: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        doc_access: 1 << 4,
      },
    },
    doc_access_delay: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 5,
      },
    },
    doc_access_stoplinks: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 6,
      },
    },
    doc_access_traveltime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 7,
      },
    },
  },

  iwa_test: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_commute_high_school: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 1,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
    doc_access_redirect: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        doc_access: 1 << 4,
      },
    },
    doc_access_delay: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 5,
      },
    },
    doc_access_stoplinks: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 6,
      },
    },
    doc_access_traveltime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 7,
      },
    },
    doc_access_sales: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        econ_api: 1,
        doc_access: 1 << 8,
      },
    },
    doc_access_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 9,
      },
    },
    doc_access_monthly_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 10,
      },
    },
    doc_access_apc: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 11,
      },
    },
    doc_access_realtime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        rt_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 12,
      },
    },
    doc_access_special_passenger_services: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        doc_access: 1 << 14,
      },
    },
    doc_access_special_passenger_services_ntd: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        doc_access: 1 << 15,
      },
    },
    doc_access_validation_report: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        econ_api: 1,
        doc_access: 1 << 16,
      },
    },
    doc_access_sps_coverage: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        sps_api: 1,
        doc_access: 1 << 17,
      },
    },
    doc_access_sps_travelers: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        scb_api: 1,
        sps_api: 1,
        doc_access: 1 << 18,
      },
    },
  },

  iwa: {
    doc_access_commute_work: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 0,
      },
    },
    doc_access_commute_high_school: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        doc_access: 1 << 1,
      },
    },
    doc_access_employment: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 2,
      },
    },
    doc_access_population: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        geo_api: 1,
        scb_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 3,
      },
    },
    doc_access_redirect: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        doc_access: 1 << 4,
      },
    },
    doc_access_delay: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 5,
      },
    },
    doc_access_stoplinks: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 6,
      },
    },
    doc_access_traveltime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        rt_api: 1,
        doc_access: 1 << 7,
      },
    },
    doc_access_sales: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        econ_api: 1,
        doc_access: 1 << 8,
      },
    },
    doc_access_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 9,
      },
    },
    doc_access_monthly_validations: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 10,
      },
    },
    doc_access_apc: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 11,
      },
    },
    doc_access_realtime: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        rt_api: 1,
        gtfs_api: 1,
        doc_access: 1 << 12,
      },
    },
    doc_access_special_passenger_services: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        doc_access: 1 << 14,
      },
    },
    doc_access_special_passenger_services_ntd: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 1,
        doc_access: 1 << 15,
      },
    },
    doc_access_validation_report: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
        econ_api: 1,
        scb_api: 1,
        doc_access: 1 << 16,
      },
    },
    doc_access_sps_coverage: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        sps_api: 1,
        doc_access: 1 << 17,
      },
    },
    doc_access_sps_travelers: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        scb_api: 1,
        sps_api: 1,
        doc_access: 1 << 18,
      },
    },
  },

  traffic_lts: {
    gtfs_api_access: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        gtfs_api: 1,
      },
    },
  },

  rebus_validation: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rebus_validation_api: 3,
      },
    },
  },

  orders_test: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        entity_api: 3,
        task_api: 3,
        message_api: 3,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        entity_api: 3,
        task_api: 3,
        message_api: 3,
      },
    },
  },

  orders_jlt: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        entity_api: 3,
        task_api: 3,
        message_api: 3,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        entity_api: 3,
        task_api: 3,
        message_api: 3,
      },
    },
  },

  hamta99: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        hamta99_api: 3,
      },
    },
  },

  dashboard: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
      },
    },
  },

  disruption_admin: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
      },
    },
  },

  disruption_admin_bt: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
      },
    },
  },

  disruption_admin_test: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
      },
    },
  },

  stop_analysis_vl: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        some_api: 1,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        some_api: 3,
      },
    },
  },

  route_redirection_vl: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
      },
    },
  },
  route_redirection_test: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        doc_api: 3,
        gtfs_api: 1,
      },
    },
  },

  realtime_transit_map_test: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rt_api: 1,
        gtfs_api: 1,
      },
    },
  },

  realtime_transit_map_vl: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        rt_api: 1,
        gtfs_api: 1,
      },
    },
  },

  some_vl: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        wo_api: 3,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        wo_api: 3,
      },
    },
  },

  bulletin_jlt: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        bulletin_api: 3,
        gtfs_api: 1,
      },
    },
  },

  bulletin_hlt: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        bulletin_api: 3,
        gtfs_api: 1,
      },
    },
  },

  bulletin_vl: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        bulletin_api: 3,
        gtfs_api: 1,
      },
    },
  },

  some_jlt: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        wo_api: 3,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        wo_api: 3,
      },
    },
  },

  some_lts: {
    entrep_saps: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        wo_api: 3,
        entrep: 1 << 0,
      },
    },
    entrep_tt: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        wo_api: 3,
        entrep: 1 << 1,
      },
    },
    boss: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        wo_api: 3,
        boss: true,
      },
    },
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        wo_api: 3,
      },
    },
  },

  senseview_staging: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
    ondemand_admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 3,
      },
    },
    ondemand_user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 1,
      },
    },
  },

  senseview_demo: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
    ondemand_admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 3,
      },
    },
    ondemand_user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 1,
      },
    },
  },

  senseview_wexnet: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_bjarekraft: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_consid: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_ewf: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_kalmarenergi: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_vanerenergi: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_karlstad: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_umeaenergi: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_hoganasenergi: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
    ondemand_admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 3,
      },
    },
    ondemand_user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 1,
      },
    },
  },

  senseview_halmstadsstadsnat: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_emmabodaenergi: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_smekab: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  senseview_ondemand: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
    ondemand_admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 3,
      },
    },
    ondemand_user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        ondemand_qr_api: 1,
      },
    },
  },

  senseview_wallenstam: {
    admin: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        admin: true,
        senseview_api: 3,
      },
    },
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
        senseview_api: 1,
      },
    },
  },

  url_shortener: {
    user: {
      required_claims: {
        admin: true,
      },
      granted_claims: {
        login: true,
      },
    },
  },
};
