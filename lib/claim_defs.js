module.exports = {
    admin: {
        type: "boolean",
    },
    super_admin: {
        type: "boolean",
    },
    allbinary_admin: {
        type: "boolean"
    },
    login: {
        type: "boolean",
    },

    doc_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    econ_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    geo_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    gtfs_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    rt_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    scb_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    sso_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    rebus_validation_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    some_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    entity_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    task_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    message_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    sps_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    wo_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },
    bulletin_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },

    doc_access: {
        type: "bitfield",
        bits: 19,
        keys: {
            0: "doc_access_commute_work",
            1: "doc_access_commute_high_school",
            2: "doc_access_employment",
            3: "doc_access_population",
            4: "doc_access_redirect",
            5: "doc_access_delay",
            6: "doc_access_stoplinks",
            7: "doc_access_traveltime",
            8: "doc_access_sales",
            9: "doc_access_validations",
            10: "doc_access_monthly_validations",
            11: "doc_access_apc",
            12: "doc_access_realtime",
            14: "doc_access_special_passenger_services",
            15: "doc_access_special_passenger_services_ntd",
            16: "doc_access_validation_report",
            17: "doc_access_sps_coverage",
            18: "doc_access_sps_travelers"
        }
    },

    entrep: {
        type: "bitfield",
        bits: 3,
        keys: {
            0: "entrep_saps",
            1: "entrep_tt",
            2: "entrep_edens"
        }
    },

    boss: {
        type: "boolean"
    },

    rebus_storage_api: {
      type: "bitfield",
      bits: 2,
      keys: {
        0: "read",
        1: "write",
      },
    },

    hamta99_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },

    senseview_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write"
        }
    },

    ondemand_qr_api: {
        type: "bitfield",
        bits: 2,
        keys: {
            0: "read",
            1: "write",
        }
    },

    psp_api: {
      type: 'bitfield',
      bits: 2,
      keys: {
        0: "read",
        1: "write"
      },
    },
};
